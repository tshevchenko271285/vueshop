import { reactive, computed, watch } from 'vue';
import { defineStore } from 'pinia';
import { useRouter } from 'vue-router';
import type Product from "@/models/Product";

/**
 * Minimum quality of item in the cart
 */
const MINIMUM_QUALITY: number = 1;

export const useCartStore = defineStore('cart', () => {
  const router = useRouter();

  const state = reactive({
    items: <Product[]>[],
  });

  /**
   * Returns count of products in the cart
   */
  const count = computed(
    (): number => state.items.reduce(
      (accumulator: number, currentItem: Product) => accumulator += currentItem.count,
      0
    )
  );

  /**
   * Redirects to the list of products if the count of products in the cart is zero.
   */
  watch(
    count,
    (newCount: number): void => {
      if (newCount === 0) {
        router.push({name: 'products'})
      }
    }
  );

  /**
   * Returns total price of the cart
   */
  const total = computed(
    (): number => state.items.reduce(
      (accumulator: number, currentItem: Product) => {
        return accumulator += (currentItem.price * currentItem.count)
      },
      0
    )
  );

  /**
   * Adds product to the cart
   * @param product
   */
  function add(product: Product): void {
    const stateProduct = state.items.find(item => item.id === product.id);
    if (stateProduct) {
      stateProduct.count = product.count + stateProduct.count;
    } else {
      state.items.push(product);
    }
  }

  /**
   * Removes item from the cart
   * @param product
   */
  function remove(product: Product): void {
    state.items = state.items.filter(item => item.id !== product.id);
  }

  /**
   * Remove all products from the cart
   */
  function clear(): void {
    state.items = [];
  }

  /**
   * Increases the number of item in the cart.
   */
  function countPlus(item: Product): void {
    const product = state.items.find(product => product.id === item.id);

    if(product) {
      product.count++;
    }
  }

  /**
   * Reduces the number of items in the cart.
   */
  function countMinus(item: Product): void {
    const product = state.items.find(product => product.id === item.id);
    if (product && product.count > MINIMUM_QUALITY) {
      product.count--;
    }
  }

  return { state, count, total, add, remove, clear, countPlus, countMinus }
})
