import { reactive } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import type Product from "@/models/Product";

export const useProductsStore = defineStore('products', () => {
  const state = reactive({
    loadingProducts: <boolean>false,
    loadingProduct: <boolean>false,
    loadingCategories: <boolean>false,
    perPage: <number>3,
    skip: <number>0,
    total: <number>0,
    selectedCategory: <string>'',
    categories: <string[]>[],
    products: <Product[]>[],
    product: <Product | null>null,
  });

  /**
   * Loads all categories
   */
  function loadCategories() {
    state.loadingCategories = true;
    axios.get('https://dummyjson.com/products/categories')
        .then(response => {
          state.loadingCategories = false;
          state.categories = response.data
        }).catch(() => state.loadingCategories = false);
  }

  /**
   * Sets selected category and reloads products
   * @param category
   */
  function setCategory(category: string) {
    state.selectedCategory = category;
    state.skip = 0;
    loadProducts();
  }

  /**
   * Change pagintation and loads products
   * @param skip
   */
  function changePage(skip: number) {
    state.skip = skip;
    loadProducts();
  }

  /**
   * Loads all products
   */
  function loadProducts() {
    state.loadingProducts = true;
    let url = 'https://dummyjson.com/products';

    if (state.selectedCategory.length) {
      url += `/category/${state.selectedCategory}`;
    }

    url += `?limit=${state.perPage}&skip=${(state.skip)}`;

    axios.get(url)
      .then(response => {
        state.products = response.data.products;
        state.skip = response.data.skip;
        state.total = response.data.total;
        state.loadingProducts = false;
      }).catch(() => state.loadingProducts = false);
  }

  /**
   * Loads product by ID
   * @param id
   */
  function loadProduct(id: number) {
    state.loadingProduct = true;
    axios.get(`https://dummyjson.com/products/${id}`)
      .then(response => {
        state.product = response.data;
        state.loadingProduct = false;
      }).catch(() => state.loadingProduct = false);
  }

  return { state, loadCategories, loadProducts, loadProduct, setCategory, changePage }
})
