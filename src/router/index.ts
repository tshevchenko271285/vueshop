import { createRouter, createWebHistory } from 'vue-router'
import Products from '../views/Products.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'products',
      component: Products
    },
    {
      path: '/product/:id',
      name: 'product',
      props: (route) => { return { id: Number.parseInt(route.params.id) } },
      component: () => import('../views/Product.vue'),
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import('../views/Cart.vue'),
    },
  ]
})

export default router
