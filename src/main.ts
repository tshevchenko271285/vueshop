import {createApp, provide, ref} from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

const app = createApp(App)

const theme = ref('light')
app.provide('theme', { theme, toggleTheme });

function toggleTheme(): void {
  theme.value = theme.value === 'dark' ? 'light' : 'dark';
}

app.use(createPinia())
app.use(router)

app.mount('#app')
