interface  Product {
  id: number;
  title: string;
  brand: string;
  category: string;
  description: string;
  price: number;
  rating: number;
  stock: number;
  thumbnail: string;
  count: number;
}

export default Product;